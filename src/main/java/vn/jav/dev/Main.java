package vn.jav.dev;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("MyScence.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("index.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("bong88");
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
        }
    }

    public static void main(String... strings) {
        launch(strings);
    }

}
